<?php

/*
 * Created on 03/02/2012
 *
 */

function services_mock_add_return_form($form, $id_mock) {
	$form = array();
	if (!isset($id_mock) || !is_numeric($id_mock)) {
		$sql_get_mock = "SELECT `id`, `title` FROM {mock_mock} ORDER BY id";
		$result = db_query($sql_get_mock);
		$options_mocks = array ();
		while($mocks = db_fetch_array($result)) {
  		$options_mocks[$mocks['id']] = t($mocks['title']);
		}

		$form['new_mock_return']['choose_mock'] = array (
			'#type' => 'select',
			'#description' => t('Select a Mock'),
			'#title' => t('Mock'),
			'#options' => array('' => t('Select One')) + $options_mocks,
			'#required' => TRUE,
      '#default_value' => (isset($form['#post']['choose_mock'])) ? $form['#post']['choose_mock'] : '',
		);

	}
	else {
		$sql_get_mock = "SELECT `title`, `note`,`status` FROM {mock_mock} " .
									   "WHERE `id`= " . $id_mock . " ORDER BY id";
		$result = db_query($sql_get_mock);
		$options_mocks =  db_fetch_array($result);

		$form['new_mock_return']['form_mock_title'] = array (
			'#type' => 'item',
			'#value' => '<label>' . t('Mock Title:') . '</label>' . $options_mocks['title'],
		);

		$form['mock_id'] = array(
	  '#type' => 'hidden',
	  '#value' => $id_mock,
	);
	}
	$sql_get_return_type = "SELECT `id`, `title`, `note` FROM {mock_return_type} ORDER BY `title`";
	$result = db_query($sql_get_return_type);
	$options_mock = array ();
	while ($return_types = db_fetch_array($result)) {
		$options_return_type[$return_types['id']] = $return_types['title'];
	}

	$form['new_mock_return']['form_mock_return'] = array (
		'#type' => 'select',
		'#description' => t('Select a Return Type'),
		'#title' => t('Return Type'),
		'#options' => array('' => t('Select One')) + $options_return_type,
		'#required' => TRUE,
    '#default_value' => (isset($form['#post']['form_mock_return'])) ? $form['#post']['form_mock_return'] : '',
	);

	$form['new_mock_return']['return_title'] = array (
		'#type' => 'textfield',
		'#title' => t('Title'),
		'#required' => TRUE,
    '#default_value' => (isset($form['#post']['return_title'])) ? $form['#post']['return_title'] : '',
  );

	$form['new_mock_return']['return_note'] = array (
		'#type' => 'fieldset',
		'#title' => t('Notes'),
		'#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#default_value' => (isset($form['#post']['return_note'])) ? $form['#post']['return_note'] : '',
  );

  $form['new_mock_return']['return_note']['note'] = array (
    '#type' => 'textarea',
    '#default_value' => (isset($form['#post']['note'])) ? $form['#post']['note'] : ''
  );

	$form['new_mock_return']['return_success'] = array (
		'#type' => 'textarea',
		'#title' => t('Success'),
		'#required' => TRUE,
    '#default_value' => (isset($form['#post']['return_success'])) ? $form['#post']['return_success'] : '',
  );

	$form['new_mock_return']['return_failure'] = array (
		'#type' => 'textarea',
		'#title' => t('Failure'),
		'#required' => TRUE,
    '#default_value' => (isset($form['#post']['return_failure'])) ? $form['#post']['return_failure'] : '',
	);

	$form['new_mock_return']['return_type_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

	return $form;
}

function services_mock_add_return_form_submit($form, &$form_state) {
  $mock_id = isset($form_state['values']['mock_id']) ? $form_state['values']['mock_id'] : $form_state['values']['choose_mock'];

  $status = 1; // Default Value
	$mock_title = trim($form_state['values']['form_mock_title']);
	$return_type_code = $form_state['values']['form_mock_return'];
	$return_title = $form_state['values']['return_title'];
	$note = trim($form_state['values']['note']);
	$success = trim($form_state['values']['return_success']);
	$failure = trim($form_state['values']['return_failure']);

	$data = array (
		'mock_id' => $mock_id,
		'mock_return_type_id' => $return_type_code,
		'title' => $return_title,
		'note' => $note,
		'success' => $success,
		'failure' => $failure,
	);

	$add_return_save = drupal_write_record('mock_return', $data);

	if ($add_return_save !== FALSE) {

	  if ($status == 1) {
		  $sql_get_return_id = "SELECT `id` FROM {mock_return} WHERE title='" .$return_title."'";
		  $result_return_id = db_query($sql_get_return_id);
			$return_type_id = db_fetch_array($result_return_id);
		  $data2 = array(
				'id' => $mock_id,
				'status' => $status,
				'return_id' => $return_type_id['id'],
			);

			$add_mock = drupal_write_record('mock_mock', $data2,'id');
			if ($add_mock !== FALSE) {
        $_SESSION['mock_collapsed_id'] = $mock_id;
				drupal_set_message(t('Return Type for mock added successfully!'));
        drupal_goto('admin/settings/mock');
			}
	  }
	}
}
