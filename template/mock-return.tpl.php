<?php
/*
 * Created on 08/02/2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

<?php print $form; ?>
<div id= 'return_list_div'>
<table>
<tr>
  <th>Id</th>
  <th>Title</th>
  <th>Return Type</th>
</tr>
<?php $count = FALSE; ?>
<?php foreach($return_list as $return) { ?>
<tr class ="<?php if ($count == TRUE) { ?>
	<?php print 'even';  ?>
	<?php  } else {?>
	<?php print 'odd';?>
	<?php }?>
	" >
  <td>
    <?php print $return['id'];  ?>
  </td>
  <td>
    <?php print $return['title'];  ?>
  </td>
  <td>
    <?php print $return['mock_return_type'];  ?>
  </td>
  <td>
    <?php print l(t('edit'), $base_path . 'admin/settings/mock/return/edit-return/' . $return['id'], array('attributes' => array('title' => t($return['title']))) ); ?>
  </td>
  <td>
    <?php print l(t('delete'), $base_path . 'admin/settings/mock/return/remove-return/' . $return['id'], array('attributes' => array('title' => t($return['title']))) ); ?>
  </td>
</tr>
<?php $count = !$count;?>
<?php }?>
</table>
</div>

