<?php
// $Id$

/**
 * @file modal-return-type.tpl.php
 */
?>
<h2 title="<?php print t('Mock Return Type') . drupal_strtoupper($content['type_return']); ?>"><?php print t('Mock Return Type ') . '(' . drupal_strtoupper($content['type_return']) . ')'; ?></h2>
<hr align="center" size="2" width="100%" /><br />

<h2 title="<?php print t('Mock Selected: ') ?>"><?php print t('Mock Selected: ') ?></h2>
<h3 title="<?php print $content['mock_title']; ?>"><?php print $content['mock_title']; ?></h3>
<br />

 <?php // Success Code ?>
  <fieldset class="mock-return-code collapsible collapsed">
    <legend class="collapse-processed">
      <a href="javascript:void(0);"><?php print t('Success'); ?></a>
    </legend>
    <div><pre class="code" lang="<?php print $content['type_return'] ?>"><?php print $content['success']; ?></pre></div>
  </fieldset>

 <?php // Failure Code  ?>
  <fieldset class="mock-return-code collapsible collapsed">
    <legend class="collapse-processed">
      <a href="javascript:void(0);"><?php print t('Failure'); ?></a>
    </legend>
    <div><pre class="code" lang="<?php print $content['type_return'] ?>"><?php print $content['failure']; ?></pre></div>
  </fieldset>





