$(document).ready(function() {

  // [For All Mocks] - Set Active / Inactive Status
  $('.check-all-mock-status').change(function(element) {
    var valueSelected = element.currentTarget.value;
    mock_list_set_status($(this), '', '', 'all_mocks', valueSelected);
  });

  // [For Mock Selected] - Set Active / Inactive Status
  $('.list-mock-status').change(function(element) {
    var id_mock       = $(this).parents('tr').children('td.col0').children('input.list-id-mock').val();
    var id_return     = '';
    var valueSelected = element.currentTarget.value;

    mock_list_set_status($(this), id_mock, id_return, 'mock', valueSelected);
  });


  // [For Active Return] - Set Mock Active
  $('.list-mock-return-type-active').change(function(element) {
    var id_mock         = $(this).parents('tr').children('td.col0').children('input.list-id-mock').val();
   var valueSelected    = $(this).find(':selected').val();

    mock_list_set_status((this), id_mock, '', 'active_return', valueSelected);
  });
 

  // [For Mock Return Status] - Set Success / Failure / Random
  $('.list-mock-return-type-status').change(function(element) {
    var id_mock         = $(this).parents('tr').children('td.col0').children('input.list-id-mock').val();
    var id_return_type  = $(this).parents('tr').children('td.col2').children('div select.list-mock-return-type-active').find(':selected').val();
    var valueSelected   = element.currentTarget.value;

    mock_list_set_status((this), id_mock, id_return_type, 'status_return_type', valueSelected);
  });


  $('ul.mock_return_types li a.remove-return').click(function(eve) {
    eve.preventDefault();

    var args = new Array();
    args['!return'] = $(this).parents('li').children('a.return-type-content').attr('title');

    var confirm_content = Drupal.t('!return will be removed. ',  args) + Drupal.t('Do you want to continue?');
    if (confirm(confirm_content)) {
      location.href = $(this).attr('href');
      return true;
    }
    return false;
  });

	// Load modal return content (success/failure) of mock returns
	$('ul.mock_return_types li a.return-type-content').click(function (eve) {
    eve.preventDefault();

    var url = Drupal.settings.base_path + $(this).attr('href').substring(1, $(this).attr('href').length);
    $.ajax({
      url : url,
      dataType : "text",
      type: "GET",
      async: false,
      success: function(data) {
        $('#return_type_modal').modal();
        $('div#return_type_modal div.content_modal').empty().html(data);

        // fieldset click action
        $("fieldset.mock-return-code legend").click(function() {
          $(this).parent().toggleClass('collapsed');
        });

        // Code Highlight for (Success and Failure Fields)
        $('pre.code').highlight({
          source:1,
          zebra:1,
          indent:'space'
        });
      }

    });
	});

});


/*
 * Implementation of AJAX function - mock_list_set_status().
 * Set Active/ Inactive Status for mock or Return Types
 * @param <object> element (The current element executed. Use " this "
 * @param <string> check_type_status  (type to change status)
 * @param <integer>  status   (0 or 1)
 */
function mock_list_set_status(element, id_mock, id_return, check_type_status, status) {
  $.ajax({
    url : Drupal.settings.base_path + 'mock-list/set-active-status/js',
    dataType : "json",
    type: "POST",
    async: false,
    data: {
      check_type_status: check_type_status,
      status: status,
      id_mock: id_mock,
      id_return: id_return
    },
    success: function(data) {
      if (data.response.success) {

        var message_changed = 'Content ';
        switch(check_type_status) {
          case 'all_mocks' :
            message_changed = 'STATUS ALL MOCKS ';
            break;
          case 'mock' :
            message_changed = 'Mock - STATUS ';
            break;
          case 'active_return' :
            message_changed = 'Mock - ACTIVE RETURN ';
            break;
          case 'status_return_type' :
            message_changed = 'Mock - STATUS ';
            break;
        }

        var fieldset_current = $(element).parents('fieldset');

        // collapsed all fieldset, less the modified file
        $('div.mock-list fieldset').each(function(i) {
          if ($(this).attr('id') != fieldset_current.attr('id')) {
            $(this).attr("class", "collapsible collapsed").children('div.fieldset-wrapper').hide().children('div.message-status-changed').hide();
          }
          else {
            $(this).attr("class", "collapsible").children('div.fieldset-wrapper').show();
          }
        });

        // Shows the change message
        var div_message_status = fieldset_current.children('div.fieldset-wrapper').children('div.message-status-changed');
        div_message_status.css('display', 'block').html(message_changed + Drupal.t('changed successfully'));
      }
    },
    error: function(data) {
      var div_message_status = $(element).parents('fieldset').children('div.fieldset-wrapper').children('div.message-status-changed');
      div_message_status.css('display', 'none').text('');
    }
  });
}

