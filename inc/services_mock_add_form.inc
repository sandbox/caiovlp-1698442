<?php
/**
 * Mock admin inc file.
 */

/**
  * Implements mock admin form
  */
function services_mock_add_form() {
$form = array();
// fieldset for including new mock
$form['new_mock'] = array(
  '#type' => 'fieldset',
  '#title' => t('New mock'),
  '#collapsible' => TRUE,
  '#collapsed' => FALSE,
);

$sql_get_category = "SELECT `id`, `title`, `note` FROM {mock_category} ORDER BY `title`";
$result = db_query($sql_get_category);

$options       = array();
$rows_selected = db_affected_rows();

if ($rows_selected > 0) {
  while ($categories = db_fetch_array($result)) {
    $options[$categories['id']] = t($categories['title']);
  }
}
else {
  $url_category = l('Category', 'admin/settings/mock/category');
  drupal_set_message(t('There are no categories registered. Please add new Category in !url', array('!url' => $url_category)), 'error');
  return $form;
}

$form['new_mock']['form_mock_category'] = array(
  '#type' => 'select',
  '#description' => t('Category which mock belongs to. Use to group mocks together.'),
  '#title' => '<span>* </span>' . t('Category'),
  '#options' => array('' => t('Select One')) + $options,
);

$form['new_mock']['form_mock_title'] = array(
  '#type' => 'textfield',
  '#description' => t('The name you will use in your code. Do not use spaces.'),
  '#title' => '<span>* </span>' . t('Title'),
  '#size' => 60,
  '#maxlength' => 200,
  '#element_validate' => array('form_validate_mock_title_unique'),
);

$form['new_mock']['notes'] = array(
  '#type' => 'fieldset',
  '#title' => t('Note'),
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
);

$form['new_mock']['notes']['form_mock_note'] = array(
  '#type' => 'textarea',
  '#description' => t('Observation Notes'),
  '#title' => t('Observation Notes'),
  '#cols' => 60,
  '#rows' => 8,
);

$form['new_mock']['form_mock_submit'] = array(
  '#type' => 'submit',
  '#value' => t('Save'),
);

return $form;
}

/**
 * Hook Validate the add_form
 * @param <type> $form
 * @param <type> $form_state
 */
function services_mock_add_form_validate($form, &$form_state) {
  if (empty($form_state['values']['form_mock_category'])) {
    form_set_error('form_mock_category', t('Please select one category'));
  }

  module_load_include('inc', 'services_mock', 'services_mock');
  if (empty($form_state['values']['form_mock_title']) || !services_mock_is_valid_alphanumeric($form_state['values']['form_mock_title'])) {
    form_set_error('form_mock_title', t('Please enter a valid Title'));
  }

}

/**
 * Hook Submit the add_form
 * @param <type> $form
 * @param <type> $form_state
 */
function services_mock_add_form_submit($form, &$form_state) {
  $category_id = $form_state['values']['form_mock_category'];
  $title = trim($form_state['values']['form_mock_title']);
  $status = 0;
  $note = $form_state['values']['form_mock_note'];

  $data = array(
    'category_id' => $category_id,
    'title' => $title,
    'note' => $note,
    'status' => $status,
  );

  $save_mock_add_table = drupal_write_record('mock_mock', $data);

  if ($save_mock_add_table !== FALSE) {
		$mock_id_insert = db_last_insert_id('mock_return', 'id');
    $_SESSION['mock_collapsed_id'] = $mock_id_insert;
    drupal_set_message(t('Mock added successfully!'), 'status');
    drupal_goto('admin/settings/mock');
  }
}

function form_mocs() {
  $form['new_mock']['form_response'] = array(
    '#type' => 'textarea',
    '#description' => t('Response'),
    '#title' => t('Response'),
    '#required' => TRUE,
    '#cols' => 60,
    '#rows' => 8,
  );
}
