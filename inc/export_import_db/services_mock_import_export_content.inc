<?php
// $Id$

/**
 * Import / Export Mock Content functions
 * @file
 */

/**
 *  Implementation of services_mock_submenu_list
 */
function _services_mock_submenu_list() {
  drupal_goto('admin/settings/mock/options/export');
}


/**
 * Get the collumns field names of var $table
 * Implementation of _services_mock_return_collumns_table
 *
 * @param <string> $table (name of table)
 * @return <string> $sql_collumns_table;
 */
function _services_mock_return_collumns_table($table) {
  $sql_collumns_table = "";
  $columns = db_query("SHOW COLUMNS FROM  `" . $table . "`");
  while ($column = db_fetch_array($columns)) {
    $sql_collumns_table .= "`" . $column['Field'] . "`,";
  }

  $sql_collumns_table = substr($sql_collumns_table, 0, -1);
  return $sql_collumns_table;
}

/**
  *  Get the SQL structure for insert all content of var $table
 * Implementation of _services_mock_export_content_table_sql
 *
 * @param <string>  $table (name of table)
 * @return <string> $sql_insert_table (Command SQL used for insert content in the "$table"
 */
function _services_mock_export_content_table_sql($table, $sql_where = NULL) {
  $header_table           = "/* Data for the table `" . $table . "` */\n";
  $return_collumns_table  = _services_mock_return_collumns_table($table); // Get name of fields table
  $sql_insert_table       = $header_table;

  // Get All values inserted into the $table
  $sql = (is_null($sql_where)) ?  "SELECT * FROM {$table} ORDER BY id" : "SELECT * FROM {$table} WHERE $sql_where ORDER BY id";
  $data = db_query($sql);

  if (!$data) {
    return '';
  }

  while ($row = db_fetch_array($data)) {
    $items = array();
    foreach ($row as $key => $value) {
      $items[] = is_null($value) ? "null" : "'". db_escape_string($value) ."'";
    }
    if ($items) {
      $sql_insert_table .=  "INSERT INTO `" . $table . "`(" . $return_collumns_table . ") VALUES(" . implode(",", $items) . ");\n";
    }
  }

  $sql_insert_table = substr($sql_insert_table, 0, -1) . "\n";
  return $sql_insert_table;
}

/**
 * Implementation of _services_mock_save_file_exported
 * Export content SQL formated to SQL File
 * @param <type> $filename (Name to save)
 * @param <type> $content_export (Text SQL formated)
 */
function _services_mock_save_file_exported($filename = NULL, $content_export = NULL) {
  if (is_null($filename) || is_null($content_export)) {
    drupal_set_message('Unable to save the file!', 'error');
    return '';
  }

  drupal_set_header("Content-Type: text/x-sql");
  drupal_set_header('Expires: '. gmdate('D, d M Y H:i:s') .' GMT');
  drupal_set_header("Content-Length:" . strlen($content_export));
  drupal_set_header('Content-Disposition: attachment; filename="' . $filename . '"');
  drupal_set_header("Content-Transfer-Encoding: binary");
  drupal_set_header('Pragma: no-cache');
  print $content_export;
  exit;
}

/**
 * Implementation of _services_mock_import_file_content
 * Import content SQL formated to mock tables
 * @param <string> $filepath
 */
function _services_mock_import_file_content($filepath = NULL) {
  if (is_null($filepath)) {
    drupal_set_message(t("File not found"), 'error');
  }
  elseif ($handle = fopen($filepath, "r")) {  // Open the file
    $num = 0;

    // Read one line at a time and run the query.
    while ($line = fgets($handle)) {

      $line = trim($line);
      // remove comments lines
      if (preg_match('/^[\/][*](.*?)[*][\/]$/', $line)) {
        continue;
      }
      if ($line) {
        // if record already exists in the database
        if($result = @_db_query($line) === FALSE) {
          drupal_set_message(t("The line #%line# cann't be imported, because the rows already exist in the database!", array("%line" =>$line)), 'error');
        }
        else {
          $num++;
        }
      }
    }
    fclose($handle);

    if ($num > 0) {
      $message = t("Import Mock content tables complete. %num SQL commands executed.", array("%num" => $num));
      drupal_set_message($message);
    }

    // Delete the file if it is temporary.
    unlink($filepath);
  }
  else {
    drupal_set_message(t("Unable to open file %file to restore database", array("%file" => $filepath)), 'error');
  }
}
