<?php
// $Id$

/**
 * Mock for Service Down Return.
 * @file
 */

/**
 * Implementation of hook_FORM
 * @param <type> $form
 * @return <type>
 */
function services_mock_service_down_form($form) {
  $form = array();

  $form['mock_service_down_title'] = array(
    '#type'  => 'markup',
    '#value' => '<h2>' . t('Service Down Return') . '</h2>',
  );

  $service_down_content = variable_get('SERVICE_DOWN_CONTENT', array(''));
  $service_down_content = is_null($service_down_content) ? '' : $service_down_content;

  $form['mock_service_down_status'] = array(
    '#type'    => 'select',
    '#title'   => t('Status  Service Down'),
    '#options' => array(
      0 => t('Off'),
      1 => t('On'),
    ),
    '#required'      => TRUE,
    '#default_value' => isset($form['#post']['mock_service_down_status']) ?  $form['#post']['mock_service_down_status'] : $service_down_content['status'],
    '#attributes'    => array('class' => 'check-all-mock-status'),

  );

  $form['mock_service_down_return'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Content Return'),
    '#description'   => t('This return is generic for all mocks'),
    '#cols'          => 60,
    '#rows'          => 12,
    '#required'      => TRUE,
    '#default_value' => isset($form['#post']['mock_service_down_return']) ?  $form['#post']['mock_service_down_return'] : $service_down_content['return'],
  );

  $form['mock_service_down_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Implementation of hook_FORM_SUBMIT.
 * @param <type> $form
 * @param <type> $form_state
 */
function services_mock_service_down_form_submit($form, &$form_state) {
  $service_down_status = $form_state['values']['mock_service_down_status'];
  $service_down_return = trim($form_state['values']['mock_service_down_return']);

  $service_down_content = array(
    'status' => $service_down_status,
    'return' => $service_down_return,
  );

  variable_set('SERVICE_DOWN_CONTENT', $service_down_content);
  drupal_set_message(t('Service down content added successfully!'));
}
