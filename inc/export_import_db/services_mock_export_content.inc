<?php
// $Id$

/**
 * Export Mock Content
 * @file
 */

/********* EXPORT CONTENT FORM *********************/

/**
 * Implementation of Hook_FORM()
 * @param  <array> $form_state
 * @return <array>
 */
function services_mock_export_content_form(&$form_state) {
  $form = array();

  $form['export']['title'] = array(
    '#type'  => 'markup',
    '#value' => '<h2>' . t('Export All Mocks') . '</h2>',
  );

  $form['export']['file_name'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Backup file name'),
    '#description'   => t('Do not use spaces.'),
    '#default_value' => isset($form_state['values']['file_name']) ? $form_state['values']['file_name'] : 'services_mock_backup_content',
    '#required'      => TRUE,
    '#size'          => 60,
    '#maxlength'     => 50,
  );

  $form['export']['append_timestamp'] = array(
    '#type'    => 'checkbox',
    '#title'   => '<strong>' . t('Append a timestamp') . '</strong>',
    '#default_value' => isset($form_state['values']['append_timestamp']) ? $form_state['values']['append_timestamp'] : '',
  );

  $form['export']['export_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Export All Mocks'),
  );

  return $form;
}

/**
 * Implementation of Hook_FORM_Validate()
 * @param <type> $form
 * @param <type> $form_state
 */
function services_mock_export_content_form_validate($form, &$form_state) {
  $file_name        = $form_state['values']['file_name'];

  module_load_include('inc', 'services_mock', 'services_mock');
  if (!services_mock_is_valid_alphanumeric($file_name)) {
    form_set_error('file_name', t('Please enter a valid name in field "Backup file name"'));
  }
}

/**
 * Implementation of Hook_FORM_Submit()
 * @param <type> $form
 * @param <type> $form_state
 */
function services_mock_export_content_form_submit($form, &$form_state) {
  $content_tables   = "";
  $file_name        = trim($form_state['values']['file_name']);
  $append_timestamp = $form_state['values']['append_timestamp'];

  /* tables of mock module */
  $mock_tables = array(
    array(
          'table'     => 'mock_category',
          'sql_where' => 'id <> 1',
         ),
    array('table' => 'mock_mock'),
    array('table' => 'mock_return'),
    array(
          'table'     => 'mock_return_type',
          'sql_where' => 'id NOT IN (1, 2, 3)',
         ),
  );

  module_load_include('inc', 'services_mock', 'inc/export_import_db/services_mock_import_export_content');
  foreach($mock_tables as $table) {
    $sql_where = isset($table['sql_where']) ? $table['sql_where'] : NULL;
    $content_tables .= _services_mock_export_content_table_sql($table['table'], $sql_where);
  }

  $file_name = empty($file_name) ? "services_mock_backup_content" : $file_name;
  if ($append_timestamp != 0) {
    $file_name .= '_' . date('Y-m-d\TH-i-s');
  }
  _services_mock_save_file_exported($file_name . '.sql', $content_tables);
}

