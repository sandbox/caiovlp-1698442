<?php
  // $Id$

  /**
  * @file mock-list.tpl.php
  *
  * Theme implementation to display a mock list content
  *
  * Available variables:
  *
  *  @mock_list_form
  *    Returns the content of mocks registered
  *    This variable is a array
  */
?>

<div class='mock-list'>
  <?php if (is_array($mock_list_form)): ?>
      <div id="message-status-changed-option">
        <?php print drupal_render($mock_list_form['messsage-status-changed-option']); ?>
      </div>

      <?php
        print drupal_render($mock_list_form['all_mock_status']);
      ?>

      <?php print drupal_render($mock_list_form['mock_category_fieldset']); ?></td>

      <div id="return_type_modal">
        <div class="content_modal"></div>
      </div>
  <?php endif; ?>
</div>
