<?php

/**
 * Implementation of services_mock_list_output
 * Returns the content of mock list
 * @return <array> $form drupal
 */
function services_mock_list_output() {
  $module_path = drupal_get_path('module', 'services_mock');

  // Addd highlight plugin for (Success / Failure) Fields
  drupal_add_css($module_path . '/js/plugin_highlight/highlight.css');
  drupal_add_js($module_path . '/js/plugin_highlight/highlight.js');

  drupal_add_css($module_path . '/js/simplemodal/css/basic.css');
  drupal_add_js($module_path . '/js/simplemodal/jquery.simplemodal.js', 'module', 'footer');

  drupal_add_js(array('base_path' => base_path()), 'setting');
  drupal_add_js($module_path . '/js/services_mock_list_mock.js', 'module', 'footer');

  $form =  drupal_get_form('services_mock_list_form');
  return $form;
}

/**
 * Implementation of Hook form
 * @param <array> $form
 * @return <array> $form
 */
function services_mock_list_form($form) {
  $form = array();
  $mocks = services_mock_list();

  if (count($mocks) == 0) {
    $form['no-mock-registered'] = array(
      '#type'   => 'markup',
      '#value'  => '<p class="no-mock-registered">' . t('There are not registered mocks') . '</p>',
    );

    return $form;
  }

  $header_titles_table = array(
    t('Mock Name'),
    t('Status'),
    t('Active Return'),
    t('Status'),
    t('All Returns')
  );

  $header_table = '<thead>';
  foreach ($header_titles_table as $key => $th) {
    $header_table .= '<th class="col' . $key . '">' . $th . '</th>';
  }
  $header_table .= '</thead>';

  $all_mock_status_value = variable_get('STATUS_ALL_MOCKS', 0);
  $all_mock_status_value = is_null($all_mock_status_value) ? 0 : $all_mock_status_value;

  $form['all_mock_status'] = array(
    '#type'    => 'select',
    '#title'   => t('Status All Mocks'),
    '#options' => array(
      0 => t('Disabled'),
      1 => t('On'),
      2 => t('Off'),
    ),
    '#default_value' => isset($form['#post']['all_mock_status']) ?  $form['#post']['all_mock_status'] : $all_mock_status_value,
    '#attributes'    => array('class' => 'check-all-mock-status'),

  );

  foreach ($mocks as $mock) {
    $form['mock_category_fieldset']['mock_category' . $mock['id']] = array(
      '#type'        => 'fieldset',
      '#title'       => check_plain(t($mock['title'])),
      '#attributes'  => array('id' => 'list-mock_category' . $mock['id']),
      '#collapsible' =>  TRUE,
      '#collapsed'   =>  TRUE,
    );

    $form['mock_category_fieldset']['mock_category' . $mock['id']]['table'] = array(
      '#type'   => 'markup',
      '#value'  => ' ',
      '#prefix' => '<div class="message-status-changed"></div><table class="mock-list-page">' . $header_table . '<tbody>',
      '#suffix' => '</tbody></table>',
    );

    $even = FALSE;
    foreach ($mock['mocks'] as $m) {
      if ($even) {
        $class_tr = 'even';
        $even = FALSE;
      }
      else {
        $even = TRUE;
        $class_tr = 'odd';
      }

      $form['mock_category_fieldset']['mock_category' . $mock['id']]['table']['mock_' . $m['id']]['id_mock'] = array(
        '#type'  => 'hidden',
        '#value' => $m['id'],
        '#attributes' => array('class' => 'list-id-mock'),
        '#prefix' => '<tr class="' . $class_tr . '"><td class="col0">',
      );

      $form['mock_category_fieldset']['mock_category' . $mock['id']]['table']['mock_' . $m['id']]['mock_title'] = array(
        '#type'   => 'item',
        '#value'  => check_plain(t($m['title'])),
        '#suffix' => '<strong>' . l(t('Add Return'), 'admin/settings/mock/return/add-new-return/' . $m['id'] , array('attributes' => array('class' => 'add-return', 'title' => 'Add Return'))) . '</strong></td>',
      );

      $form['mock_category_fieldset']['mock_category' . $mock['id']]['table']['mock_' . $m['id']]['form_mock_status_' . $m['id']] = array(
        '#type'    => 'select',
        '#options' => array(
          1 => t('On'),
          0 => t('Off'),
        ),
        '#default_value' => isset($form['#post']['form_mock_status_' . $m['id']]) ? $form['#post']['form_mock_status_' . $m['id']] : $m['status'],
        '#attributes' => array('class' => 'list-mock-status'),
        '#prefix' => '<td class="col1">',
        '#suffix' => '</td>',
      );

      $mock_returns_type = array();
      $return_types = '<ul class="mock_return_types">';
      $return_type_active_id = 0;

      module_load_include('inc', 'services_mock', 'services_mock');
      foreach ($m['return_type'] as $key => $rtype) {
        $mock_returns_type[$rtype['id']] = check_plain($rtype['title']) . ' (' . check_plain(t($rtype['type_return'])) . ')';

        if ($rtype['return_type_active']) {
          $return_type_active_id = $rtype['id'];
        }

        $return_text_title = t($rtype['title']);
        if ($text_trunc = _services_mock_truncate_check_text($rtype['title'], 25)) {
          $rtype['title']  = $text_trunc;
        }

        $return_types .= '<li>';
          $return_types .= l($rtype['title'], 'admin/settings/mock/' . $rtype['mock_id'] . '/return_type/' . $rtype['mock_return_type_id'] . '/'. $rtype['type_return'], array('attributes' => array('class' => 'return-type-content', 'title' => $return_text_title))) . ' ';
          $return_types .= l(t('Edit Return'), 'admin/settings/mock/return/edit-return/' .$rtype['id'] , array('attributes' => array('class' => 'edit-return', 'title' => 'Edit Return')));
          $return_types .= l(t('Remove Return'), 'admin/settings/mock/return/remove-return/' .$rtype['id'] , array('attributes' => array('class' => 'remove-return', 'title' => 'Remove Return')));
        $return_types .= '</li>';
      }
      $return_types .= '</ul>';

      $form['mock_category_fieldset']['mock_category' . $mock['id']]['table']['mock_' . $m['id']]['return_type_' . $m['id']] = array(
        '#type'    => 'select',
        '#options' => $mock_returns_type,
        '#default_value' => isset($form['#post']['return_type' . $m['id']]) ? $form['#post']['return_type' . $m['id']] : $return_type_active_id,
        '#attributes' => array('class' => 'list-mock-return-type-active'),
        '#prefix' => '<td class="col2">',
        '#suffix' => '</td>',
      );

      $form['mock_category_fieldset']['mock_category' . $mock['id']]['table']['mock_' . $m['id']]['return_type_status_' . $m['id']] = array(
        '#type'    => 'select',
        '#options' => array(
          'success' => t('Success'),
          'failure' => t('Failure'),
          'random' => t('Random'),
        ),
        '#default_value' => isset($form['#post']['return_type_status_' . $m['id']]) ? $form['#post']['return_type_status_' . $m['id']] : $rtype['return_status'],
        '#attributes' => array('class' => 'list-mock-return-type-status'),
        '#prefix' => '<td class="col4">',
        '#suffix' => '</td>',
      );

      $form['mock_category_fieldset']['mock_category' . $mock['id']]['table']['mock_' . $m['id']]['return_types'] = array(
        '#type'   => 'markup',
        '#value'  => $return_types,
        '#prefix' => '<td class="col5">',
        '#suffix' => '</td></tr>',
      );
    }
  }

  $form['#theme'] = 'mock-list';
  return $form;
}

/**
 * Implementation of services_mock_list_set_status
 * Set Active/ Inactive Status for all mocks, a Specific Mock or Return Types of Mock
 * Notes: If variable "$check_type_status" is "all_mocks" and your status is TRUE,
 *        it will prevail on the status of each mock
 *
 * @return <json>
 */
function services_mock_list_set_status() {
  $check_type_status = $_POST['check_type_status'];
  $status            = $_POST['status'];
  $id_mock           = (isset($_POST['id_mock']) && is_numeric($_POST['id_mock'])) ? $_POST['id_mock'] : NULL;

  $results_mock_change_status = FALSE;

  if (isset($check_type_status) && isset($status)) {

    if ($check_type_status == 'all_mocks') {  // change status for all mock
      variable_set('STATUS_ALL_MOCKS', $status);
      $results_mock_change_status = TRUE;
    }
    elseif ($check_type_status == 'status_return_type') {
      $id_return_type = (isset($_POST['id_return']) && !empty($_POST['id_return']) && is_numeric($_POST['id_return'])) ? $_POST['id_return'] : NULL;

      if (!is_null($id_return_type)) {
        $sql_update_status = "UPDATE {mock_return} SET return_status = '" . $status . "'
                              WHERE id = " . $id_return_type . " AND mock_id = " . $id_mock;
        $results = db_query($sql_update_status);
        $results_mock_change_status = TRUE;
      }
    }
    elseif ($check_type_status == 'active_return') {
      $data = array(
        'id' => $id_mock,
        'return_id' => $status,
      );

      $results_mock_change_status = drupal_write_record('mock_mock', $data, 'id');
      if ($results_mock_change_status !== FALSE) {
        $results_mock_change_status = TRUE;
      }
    }
    elseif (!is_null($id_mock)) {
      $id_return = (isset($_POST['id_return']) && !empty($_POST['id_return']) && is_numeric($_POST['id_return'])) ? $_POST['id_return'] : NULL;

      $data = array(
        'id' => $id_mock,
        'status' => $status,
      );

      if ($check_type_status == 'return_type' && !is_null($id_return)) {
        $data['return_id'] = $id_return;
      }

      $results_mock_change_status = drupal_write_record('mock_mock', $data, 'id');
      if ($results_mock_change_status !== FALSE) {
        $results_mock_change_status = TRUE;
      }
    }

  }
  return ($results_mock_change_status) ? drupal_json(array('response' => array('success' => TRUE))) : drupal_json(array('response' => array('success' => FALSE)));
}

/**
 * Implementation of services_mock_list
 * Return the content of database for list
 * @return <array> $return
 */
function services_mock_list() {
  $return = array();
  $sql_get_mocks_category = <<<QUERY
    SELECT DISTINCT mock_category.id, mock_category.title, mock_category.note from {mock_category}
    JOIN {mock_mock} ON mock_category.id = mock_mock.category_id
    ORDER BY mock_category.title ASC
QUERY;

  $results_categories = db_query($sql_get_mocks_category);

  while($result = db_fetch_array($results_categories)) {
    $return[$result['id']] = $result;
    $sql_get_all_mocks = <<<QUERY
      SELECT mock_mock.id, mock_mock.category_id, mock_mock.title, mock_mock.note, mock_mock.status, mock_mock.return_id
      FROM {mock_mock}
      WHERE mock_mock.category_id = {$result['id']}
      ORDER BY mock_mock.category_id
QUERY;
    $results_mocks = db_query($sql_get_all_mocks);
    while($mocks = db_fetch_array($results_mocks)) {
      $return[$result['id']]['mocks'][] = $mocks;
    }

    foreach($return[$result['id']]['mocks'] as $key_mock => $values) {
      // Mock return Types
      $sql_get_mock_return_types = <<<QUERY
        SELECT mr.*, mrt.title as type_return from {mock_return} AS mr
        JOIN {mock_return_type} AS mrt ON mr.mock_return_type_id = mrt.id
        WHERE mr.mock_id = {$values['id']}
        ORDER BY mrt.title
QUERY;
      $results_return_types = db_query($sql_get_mock_return_types);
      if ($results_return_types) {

        while ($return_types = db_fetch_array($results_return_types)) {
          $return_type_item =  $return_types + array('return_type_active' => FALSE);

          // if "return" is [Active]
          if (!is_null($values['return_id']) && $values['return_id'] == $return_types['id']) {
            $return_type_item['return_type_active'] = TRUE;
  
            // if "return" is [Active], then your content will be the first of list retuns
            if (is_array(isset($return[$result['id']]['mocks'][$key_mock]['return_type']))) {
              array_unshift($return[$result['id']]['mocks'][$key_mock]['return_type'], $return_type_item);
            }
            else {
              // if "return" is the first element of array
              $return[$result['id']]['mocks'][$key_mock]['return_type'][] = $return_type_item;
            }
          }
          else {
            $return[$result['id']]['mocks'][$key_mock]['return_type'][] = $return_type_item;
          }
        }

      }
    }
  }

  return $return;
}
