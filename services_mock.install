<?php

/**
 * Implementation of hook_install().
 */
function services_mock_install() {
  drupal_install_schema('services_mock');

  /**
   * Setting Global VARS
   */
  variable_set('SERVICE_DOWN_CONTENT', array());
  variable_set('STATUS_ALL_MOCKS', 0);


  /**
   * Load default values
   */  
  $sql_first_category = "INSERT INTO {mock_category} (title, note) VALUES ('Miscelaneous', 'Miscelaneous category')";
  db_query($sql_first_category);
  
  $return_types = array(
    'xml' => 'Return XML Header',
    'json' => 'Return JSON Header',
    'plain' => 'Plain text return',
  );
  foreach ($return_types as $key => $value) {
    $sql_return_types = "INSERT INTO {mock_return_type} (title, note) VALUES ('$key', '$value')";
    db_query($sql_return_types);
  }
}

/**
 * Implementation of hook_uninstall().
 */
function services_mock_uninstall() {
  drupal_uninstall_schema('services_mock');

  variable_del('TABLE_RETURN_TYPE');
  variable_del('TABLE_CATEGORY');
  variable_del('TABLE_MOCK');
  variable_del('SERVICE_DOWN_CONTENT');
  variable_del('STATUS_ALL_MOCKS');
}

/**
 * Implementation of hook_schema().
 */
function services_mock_schema() {
  $schema = array();

  $schema['mock_category'] = array(
    'fields' => array(
      'id' => array(
        'type' => 'serial',
      ),
      'title' => array(
        'description' => t('Category field title.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'size' => 'normal',
        'length' => '255',
      ),
      'note' => array(
        'description' => t('Category notes.'),
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
      ),
    ),
    'primary key' => array('id'),

    'unique keys' => array(
      'mock_category' => array('title'),
    ),
  );

  $schema['mock_return_type'] = array(
    'fields' => array(
      'id' => array(
        'type' => 'serial',
      ),
      'title' => array(
        'description' => t('Return type field title.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'size' => 'normal',
        'length' => '255',
      ),
      'note' => array(
        'description' => t('Return type notes.'),
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
      ),
    ),
    'primary key' => array('id'),

    'unique keys' => array(
      'mock_return_type' => array('title'),
    ),
  );

  $schema['mock_mock'] = array(
    'fields' => array(
      'id' => array(
        'type' => 'serial',
      ),
      'category_id' => array(
        'type' => 'int',
      ),
      'title' => array(
        'description' => t('Mock field title.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'size' => 'normal',
        'length' => '255',
      ),
      'note' => array(
        'description' => t('Mock note.'),
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'normal',
      ),
      'status' => array(
        'description' => t('Mock status.'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
      ),
      'return_id' => array(
        'description' => t('Return id for a status on return.'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => FALSE,
        'default' => NULL,
      ),
    ),
    'primary key' => array('id'),

    'unique keys' => array(
      'mock_mock' => array('title'),
    ),
  );

  $schema['mock_return'] = array(
    'fields' => array(
      'id' => array(
        'type' => 'serial',
      ),
      'mock_id' => array(
        'type' => 'int',
      ),
      'mock_return_type_id' => array(
        'type' => 'int',
      ),
      'title' => array(
        'description' => t('Mock field title.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'size' => 'normal',
        'length' => '255',
      ),
      'note' => array(
        'description' => t('Mock return note.'),
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'normal',
      ),
      'success' => array(
        'description' => t('Mock return success.'),
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'big',
      ),
      'failure' => array(
        'description' => t('Mock return fail.'),
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'big',
      ),
      'return_status' => array(
        'description' => t('Return status of Mock.'),
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'normal',
      ),
    ),
    'primary key' => array('id'),

    'unique keys' => array(
      'mock_return' => array('title'),
    ),
  );

  return $schema;
}
