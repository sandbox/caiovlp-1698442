<?php
/**
 * Mock admin inc file.
 */

/**
 * Implementation of _services_mock_category_output
 * @param <type> $form
 * @param <type> $id
 * @param <type> $action
 * @return <type>
 */
function _services_mock_category_output($form, $id = NULL, $action = NULL) {
  $form = drupal_get_form('services_mock_category_add_form', $id, $action);

  module_load_include('inc', 'services_mock', 'services_mock');
  if (count(services_mock_list_category()) == 0) {
    return $form;
  }
  return theme('mock-category', $form, services_mock_list_category());
}

/**
 * Implementation of Hook Form
 * @param <type> $form
 * @param <type> $id
 * @param <type> $action
 * @return <type>
 */
function services_mock_category_add_form($form, $id = NULL, $action = NULL) {
  $form = array();

  if (!is_null($id)) {
    $category = array();
    $category = _services_mock_get_category_by_id($id);
    $form['id'] = array(
      '#type' => 'hidden',
      '#value' => $id,
    );
    $form_collapsed = FALSE;
  } else {
    $form_collapsed = TRUE;
  }

  if (!is_null($action) &&  $action == 'verify') {
  	$form_collapsed = TRUE;
    module_load_include('inc', 'services_mock', 'inc/services_mock_category_verify');
    return services_mock_category_verify($id);
  }

  // fieldset for including new category
  $form['new_category'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add a new category'),
    '#collapsible' => TRUE,
    '#collapsed' => $form_collapsed,
  );

  $form['new_category']['form_mock_category_title'] = array(
    '#type' => 'textfield',
    '#description' => t('The name you will use for your category.'),
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => isset($category['title']) ? $category['title'] : '',
	'#size' => 60,
    '#maxlength' => 200,
    '#element_validate' => array('form_validate_mock_category_title_unique'),
  );

  $form['new_category']['form_mock_category_note'] = array(
    '#type' => 'textarea',
    '#description' => t('Observation Notes.'),
    '#title' => t('Notes'),
    '#default_value' => isset($category['note']) ? $category['note'] : '',
    '#size' => 60,
    '#maxlength' => 200,
    //'#element_validate' => array('form_validate_mock_category_note_unique'),
  );

  $form['new_category']['form_mock_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Implementation of Hook form_submit
 * @param <type> $form
 * @param <type> $form_state
 */
function services_mock_category_add_form_submit($form, &$form_state) {
  $title = $form_state['values']['form_mock_category_title'];
  $note = $form_state['values']['form_mock_category_note'];

  $data = array(
    'title' => $title,
    'note' => $note,
   );

  if (isset($form_state['values']['id'])) {
    $data['id'] = $form_state['values']['id'];
    drupal_write_record('mock_category', $data, 'id');
  }
  else {
    drupal_write_record('mock_category', $data);
  }

  $path = 'admin/settings/mock/category';
  drupal_goto($path);
  drupal_set_message(t('Category added successfully!'));
}

/**
 * Implemetation of _services_mock_get_category_by_id
 * @param <type> $id
 * @return <type>
 */
function _services_mock_get_category_by_id($id) {
  $return = array();
  $sql_get_all_mocks_categories = "SELECT `id`, `title`, `note` FROM {mock_category} WHERE id=" . $id;
  $result = db_query($sql_get_all_mocks_categories);
  $return = db_fetch_array($result);

  return $return;
 }

 /**
  * Implementation of _services_mock_delete_category_by_id
  * @param <type> $id
  */
function _services_mock_delete_category_by_id($id) {
  $return = array();
  $sql_delete_mocks_category = "DELETE FROM {mock_category} WHERE id=" . $id;
  db_query($sql_delete_mocks_category);
  drupal_set_message(t('Category deleted successfully!'));
  $path = 'admin/settings/mock/category';
  drupal_goto($path);
}

/**
 * Implementation of services_mock_category_list
 * @return <type>
 */
function services_mock_category_list() {
  $return = array();
  $sql_get_all_mocks_category = "SELECT `id`, `title`, `note`, FROM {mock_category}";
  $results = db_query($sql_get_all_mocks_category);

  while($result = db_fetch_array($results)) {
    $return[] = $result;
  }

  return $return;
}

