<?php

/**
 * Return form
 */
function services_mock_return_content($form, $param_type = NULL, $param_id = NULL) {
  $module_path = drupal_get_path('module', 'services_mock');
  return drupal_get_form($form, $param_type, $param_id);
}

/**
 * Implementation of _services_mock_return_form_output
 * @param <type> $form
 * @param <type> $param_type
 * @param <type> $param_id
 * @return <type>
 */
function _services_mock_return_form_output($form, $param_type = NULL, $param_id = NULL) {
  $form = drupal_get_form('services_mock_return_form', $param_type, $param_id);

  module_load_include('inc', 'services_mock', 'services_mock');
  if (count(services_mock_returns_list()) == 0) {
    return $form;
  }
  return theme('mock-return', $form, services_mock_returns_list());
}

/**
 * Implements Hook_Form().
 * @param type $form
 * @param type $param_type
 * @param type $param_id
 * @return string
 */
function services_mock_return_form($form, $param_type = NULL, $param_id = NULL) {
  if (!is_null($param_type)) {
    $param_type = drupal_strtolower($param_type);
  }

  if ($param_type == 'remove-return') {
    $sql_get_all_mocks = "DELETE FROM {mock_return} WHERE id =" . $param_id;
    $result = db_query($sql_get_all_mocks);
    drupal_set_message(t('Return deleted successfully!'));
    drupal_goto('admin/settings/mock/return');
  }

  $form = array();

  /**
   * Get all mocks
   * If there are no mocks don't show the for and show a messagem
   */
  $sql_get_all_mocks = "SELECT `id`, `title` FROM {mock_mock} ORDER BY `title`";
  $result = db_query($sql_get_all_mocks);
  $options_mocks = array();
  while ($mocks = db_fetch_array($result)) {
    $options_mocks[$mocks['id']] = t($mocks['title']);
  }
  if (count($options_mocks) < 1) {
    $form['no-mock-registered'] = array(
      '#type' => 'markup',
      '#value' => '<p class="no-mock-registered">' . t('Please add a mock before adding a return') . '</p>',
    );

    return $form;
  }

  $title = ($param_type == 'edit-return') ? 'Edit return' : 'New return';

  $form['mock_return_fieldset'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => (is_null($param_type)) ? TRUE : FALSE,
    '#title' => check_plain($title),
  );
  /**
   * If $param_type and $param_id are both null we show a select combo
   * with all mocks
   */

  if ($param_type == 'edit-return') {
    $_SESSION['param_type'] = $param_type;
    $sql_get_mock_return_data = "
      SELECT * FROM {mock_return} WHERE `id` = $param_id
      LIMIT 0, 1
    ";

    $result = db_query($sql_get_mock_return_data);

    $mock_return_data = db_fetch_object($result);

    if (!$mock_return_data) {
      drupal_goto('admin/settings/mock/return');
    }

    $sql_get_mock_data = "
      SELECT `id`, `title` FROM {mock_mock} WHERE `id` = $mock_return_data->mock_id
      LIMIT 0, 1
    ";
    $result = db_query($sql_get_mock_data);

    $mock_data = db_fetch_object($result);

    $form['mock_return_fieldset']['form_mock_title'] = array(
      '#type' => 'item',
      '#value' => '<label>' . t('Mock Title:') . '</label>' . $mock_data->title,
      '#weight' => -50,
    );
    $form['choose_mock'] = array(
      '#type' => 'hidden',
      '#value' => $mock_data->id,
    );
    $form['return_id'] = array(
      '#type' => 'hidden',
      '#value' => $param_id,
    );
    $form['mock_edit'] = array(
      '#type' => 'hidden',
      '#value' => 1,
    );
  }
  elseif ($param_type == 'add-new-return') {
    $sql_get_mock_data = "
      SELECT m.`id`, m.`title` FROM {mock_mock} as m WHERE m.`id` = $param_id
      LIMIT 0, 1
    ";
    $result = db_query($sql_get_mock_data);

    $mock_data = db_fetch_object($result);

    if (!$mock_data) {
      drupal_goto('admin/settings/mock/return');
    }

    $form['mock_return_fieldset']['form_mock_title'] = array(
      '#type' => 'item',
      '#value' => '<label>' . t('Mock Title:') . '</label>' . $mock_data->title,
      '#weight' => -50,
    );
    $form['choose_mock'] = array(
      '#type' => 'hidden',
      '#value' => $mock_data->id,
    );
    $form['return_id'] = array(
      '#type' => 'hidden',
      '#value' => $param_id,
    );
  }
  else {
    $sql_get_category = "SELECT `id`, `title` FROM {mock_category} ORDER BY `title`";
    $result = db_query($sql_get_category);
    while ($return_categorys = db_fetch_array($result)) {
      $options_return_category[$return_categorys['id']] = $return_categorys['title'];
    }

//    $form['mock_return_fieldset']['form_category'] = array(
//      '#type' => 'select',
//      '#description' => t('Select a Category'),
//      '#title' => t('Category'),
//      '#options' => array('' => t('Select One')) + $options_return_category,
//      '#required' => TRUE,
//      '#default_value' => ($param_type == 'edit-return' && $mock_return_data->mock_return_type_id) ? $mock_return_data->mock_return_type_id : '',
//    );

    $form['mock_return_fieldset']['choose_mock'] = array(
      '#type' => 'select',
      '#description' => t('Select a Mock'),
      '#title' => t('Mock'),
      '#options' => array('' => t('Select One')) + $options_mocks,
      '#required' => TRUE,
    );
  }

  $sql_get_return_type = "SELECT `id`, `title` FROM {mock_return_type} ORDER BY `title`";
  $result = db_query($sql_get_return_type);
  while ($return_types = db_fetch_array($result)) {
    $options_return_type[$return_types['id']] = $return_types['title'];
  }

  $form['mock_return_fieldset']['form_mock_return'] = array(
    '#type' => 'select',
    '#description' => t('Select a Return Type'),
    '#title' => t('Return Type'),
    '#options' => array('' => t('Select One')) + $options_return_type,
    '#required' => TRUE,
    '#default_value' => ($param_type == 'edit-return' && $mock_return_data->mock_return_type_id) ? $mock_return_data->mock_return_type_id : '',
  );

  $form['mock_return_fieldset']['return_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => ($param_type == 'edit-return' && $mock_return_data->title) ? $mock_return_data->title : '',
  );

  $form['mock_return_fieldset']['return_note'] = array(
    '#type' => 'fieldset',
    '#title' => t('Notes'),
    '#collapsible' => TRUE,
    '#collapsed' => ($param_type == 'edit-return' && $mock_return_data->note) ? FALSE : TRUE,
  );

  $form['mock_return_fieldset']['return_note']['note'] = array(
    '#type' => 'textarea',
    '#default_value' => ($param_type == 'edit-return' && $mock_return_data->note) ? $mock_return_data->note : '',
  );

  // Mock Return Status
  module_load_include('inc', 'services_mock', 'services_mock');
  $options_return_status = _services_mock_status_return_type();

  $form['mock_return_fieldset']['return_status'] = array(
    '#type'        => 'select',
    '#description' => t('Select a Return Status (Success / Failured / Random)'),
    '#title'       => t('Return Status'),
    '#options'     => $options_return_status,
    '#required'    => TRUE,
    '#default_value' => ($param_type == 'edit-return' && $mock_return_data->return_status) ? $mock_return_data->return_status : '',
  );

  $form['mock_return_fieldset']['return_success'] = array(
    '#type' => 'textarea',
    '#title' => t('Success'),
    '#required' => TRUE,
    '#default_value' => ($param_type == 'edit-return' && $mock_return_data->success) ? $mock_return_data->success : '',
  );

  $form['mock_return_fieldset']['return_failure'] = array(
    '#type' => 'textarea',
    '#title' => t('Failure'),
    '#required' => TRUE,
    '#default_value' => ($param_type == 'edit-return' && $mock_return_data->failure) ? $mock_return_data->failure : '',
  );

  $form['mock_return_fieldset']['return_type_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Implements Hook_Validate
 * @param type $form
 * @param type $form_state
 */
function services_mock_return_form_validate($form, &$form_state) {
  $return_type   = $form_state['values']['form_mock_return'];
  $success       = $form_state['values']['return_success'];
  $failure       = $form_state['values']['return_failure'];

  if ($return_type && ($success || $failure)) {
    $sql_get_return_type = <<<QUERY
        SELECT title
        FROM mock_return_type
        WHERE id = {$return_type}
QUERY;
      $result = db_query($sql_get_return_type);
      $return_type = db_fetch_array($result);

      if (count($return_type) == 1) {
        module_load_include('inc', 'services_mock', 'services_mock');


        // Field Success Return
        if (!_services_mock_return_check_return_type($return_type['title'], $success) && $success) {
          form_set_error('return_success', t('Success field is invalid !return_type content', array('!return_type content' => $return_type['title'])));
        }

        // Field Failure Return
        if (!_services_mock_return_check_return_type($return_type['title'], $failure) && $failure) {
          form_set_error('return_failure', t('Failure field is invalid !return_type content', array('!return_type content' => $return_type['title'])));
        }
      }
      else {
        drupal_set_message('There was an error in the database  information', 'error');
      }
  }
}

/**
 * Implements Hook_Submit
 * @param type $form
 * @param type $form_state
 */
function services_mock_return_form_submit($form, &$form_state) {
  array_walk($form_state['values'], 'trim');

  $mock_id          = $form_state['values']['choose_mock'];
  $return_type_code = $form_state['values']['form_mock_return'];
  $return_title     = $form_state['values']['return_title'];
  $note             = $form_state['values']['note'];
  $return_status    = drupal_strtolower($form_state['values']['return_status']);
  $success          = $form_state['values']['return_success'];
  $failure          = $form_state['values']['return_failure'];

  $data_return = array(
    'mock_id'       => $mock_id,
    'mock_return_type_id' => $return_type_code,
    'title'         => $return_title,
    'note'          => $note,
    'success'       => $success,
    'failure'       => $failure,
    'return_status' => $return_status,
  );

  if (isset($form_state['values']['mock_edit'])) {
    $data_return['id'] = $form_state['values']['return_id'];

    drupal_write_record('mock_return', $data_return, 'id');
  }
  else {
    $add_return_save = drupal_write_record('mock_return', $data_return);
    $status = 1; // Default Value
    $return_type_id = db_last_insert_id('mock_return', $data_return, 'id');

    $data_mock = array(
      'id' => $mock_id,
      'status' => $status,
      'return_id' => $return_type_id,
    );

    drupal_write_record('mock_mock', $data_mock, 'id');
  }

  if ($add_return_save) {
    $_SESSION['mock_collapsed_id'] = $mock_id;
    drupal_set_message(t('Return for mock added successfully!'));
    drupal_goto('admin/settings/mock');
  }
  else {
    drupal_set_message(t('Return edited successfully!'));
    drupal_goto('admin/settings/mock/return');
  }
}

