<?php
/**
 * @file
 */

/**
 * Implementation of services_mock_list_category
 * Return content of  Mock "Category"
 * @return <array> $return
 */
function services_mock_list_category() {
  $return = array();
  $sql_get_all_mocks_category = "SELECT `id`, `title`, `note` FROM {mock_category} ORDER BY `title` ASC";
  $results = db_query($sql_get_all_mocks_category);

  while($result = db_fetch_array($results)) {
    $return[] = $result;
  }

  return $return;
}

/**
 * Implementation of services_mock_return_type_list
 * Return content of  Mock "Return Type"
 * @return <array> $return
 */
function services_mock_return_type_list() {
  $return = array();
  $sql_get_all_mocks_return_types = "SELECT `id`, `title`, `note` FROM {mock_return_type} ORDER BY `title` ASC";
  $results = db_query($sql_get_all_mocks_return_types);

  while($result = db_fetch_array($results)) {
    $return[] = $result;
  }

  return $return;
}

function services_mock_returns_list() {
  $return = array();
  $sql_get_all_mocks_returns = "SELECT mr.id, mr.mock_id, mr.title, mrt.title as mock_return_type
                                  FROM {mock_return} mr
                            INNER JOIN {mock_return_type} mrt ON mrt.id = mr.mock_return_type_id
                            ORDER BY mr.title ASC";
  $results = db_query($sql_get_all_mocks_returns);

  while($result = db_fetch_array($results)) {
    $return[] = $result;
  }

  return $return;
}


/**
 * Function to validate alphanumeric strings
 * Implementation of  services_mock_is_valid_alphanumeric
 * @param string $text Text to be validate
 * @return bool
 */
function services_mock_is_valid_alphanumeric($text = NULL) {
  if (is_null($text)) {
    return FALSE;
  }
  return !(bool) preg_match("/([^\w,'\xC0-\xFF\-])/", $text);
}

/**
 * Get mock data
 * @param type $mock_title
 * @return array
 */
function _private_mock_return($mock_title, $success_failure_random = FALSE) {
  /**
   * Checking for service down variable
   */
  $service_down_content = variable_get('SERVICE_DOWN_CONTENT', array());
  if (isset($service_down_content['status']) && $service_down_content['status'] == 1) {
    $return_data = array(
      'status' => 'SERVICE_DOWN',
      'type' => 'plain',
      'data' => $service_down_content['return'],
      'message' => '[NOTICE] Return for service down state',
    );
    return $return_data;
  }

  /**
   * Check if global all mocks off is set
   */
  $all_mock_status_value = variable_get('STATUS_ALL_MOCKS', 0);
  if ($all_mock_status_value == 2) {
    $return_data = array(
      'status' => 'OFF',
      'type' => 'plain',
      'data' => (string) '',
      'message' => '[WARNING] All mocks are set off',
    );
    return $return_data;
  }

  $sql_get_mock = "
    SELECT m.title as mock_title
      ,mr.title as mock_return
      ,mr.return_status as return_status
      ,mr.success as success
      ,mr.failure as failure
      ,mrt.title as return_type

    FROM {mock_mock} as m

    JOIN {mock_return} AS mr ON m.return_id = mr.id

    JOIN {mock_return_type} AS mrt ON mr.mock_return_type_id = mrt.id

    WHERE m.title = '{$mock_title}'";

  // if option "Status All Mocks" is disabled, returns only mocks with status active
  if ($all_mock_status_value == 0) {
    $sql_get_mock .= ' and m.status = 1';
  }

  $sql_get_mock .= ' limit 0, 1';

  $resource = db_query($sql_get_mock);

  $db_return = db_fetch_object($resource);

  if (!$db_return) {
    $return_data = array(
      'status' => 'OFF',
      'type' => 'plain',
      'data' => (string) '',
      'message' => '[WARNING] This Mock either is off or do not exists',
    );
    return $return_data;
  }

  if (!$success_failure_random) {
    $success_failure_random = $db_return->return_status;
  }

  switch ($success_failure_random) {
    case 'random':
      $rand = array('success', 'failure');
      shuffle($rand);
      $sfr = $db_return->$rand[rand(0, 1)];
      break;
    case 'failure':
      $sfr = $db_return->failure;
      break;
    case 'success':
    default:
      $sfr = $db_return->success;
      break;
  }

  /**
   * Returns data from DB
   */
  $return_data = array(
    'status' => 'ON',
    'type' => $db_return->return_type,
    'data' => $sfr,
    'message' => '[SUCCESS] Data ok',
  );
  return $return_data;

}

function mockapi($mock_title, $success_failure_random = 'success') {
  $mock_data = _private_mock_return($mock_title, $success_failure_random);

  $type = $mock_data['type'];
  $data = $mock_data['data'];

  $return_type = _set_return_header($type);

  if (!$mock_data['status']) {
    switch ($type) {
      case 'json':
        $data = $data['message'];
        return '_set_json_output';
        break;
      case 'xml':
        $data = $data['message'];
        return '_set_xml_output';
        break;
      case 'plain':
      default:
        $data = $data['message'];
        return '_echo_plain';
        break;
    }
    call_user_func($return_type, $data);
  }

  call_user_func($return_type, $data);
}

/**
 * Implementation of mock
 * Function to return mock
 * @param <type> $mock_title
 */
function mock($mock_title) {
  $mock_data = _private_mock_return($mock_title);

  return $mock_data;
}

/**
 * Implementation of _set_json_output
 * Set JSON content to return
 * @param <string> $jsonstring
 * return <json> $jsonstring
 */
function _set_json_output($jsonstring) {
  drupal_set_header("Content-Type: application/json; charset=utf-8");
  print $jsonstring;
  exit;
}

/**
 * Implementation of _set_xml_output
 * Set XML content to return
 * @param <string> $xmlstring
 * return <xml> $xmlstring
 */
function _set_xml_output($xmlstring) {
  drupal_set_header('Content-type: application/xml');
  $xml = simplexml_load_string($xmlstring);
  print $xml->asXML();
  exit;
}

/**
 * Implementation of _echo_plain
 * Set "Plain text" content to return
 * @param <string> $string
 * return <plain string> $string
 */
function _echo_plain($string) {
  drupal_set_header('Content-Type:text/plain');
  print $string;
  exit;
}

/**
 * Implementation of _set_return_header
 * Return function for header "$type"
 * @param <string> $type
 * @return <function name>
 */
function _set_return_header($type) {
  switch ($type) {
    case 'json':
      return '_set_json_output';
      break;
    case 'xml':
      return '_set_xml_output';
      break;
    case 'plain':
    default:
      return '_echo_plain';
      break;
  }
}

/**
 * Implemented of mock_output
 * @return <type>
 */
function mock_output() {
  $service_down_content = variable_get('SERVICE_DOWN_CONTENT', array(''));
  if (!is_null($service_down_content) && $service_down_content['status'] == 1) {
    return '<strong>' . t('Service Down Message:') . '</strong><br />' . $service_down_content['status'];
  }

  $all_mock_status_value = variable_get('STATUS_ALL_MOCKS', 0);
  if (!is_null($all_mock_status_value) && $all_mock_status_value == 2) {
    return '<strong>' . t('The option "Status All Mocks" is "Off". To return mocks enabled, it needs to be ("disabled" or "On")') . '</strong>';
  }

  $sql_get_all_mocks = <<<QUERY
    SELECT m.title as mock_title
      ,mr.title as mock_return
      ,mr.success as success
      ,mr.failure as failure
      ,mrt.title as return_type

    FROM {mock_mock} as m

    JOIN {mock_return} AS mr ON m.return_id = mr.id

    JOIN {mock_return_type} AS mrt ON mr.mock_return_type_id = mrt.id

    WHERE m.return_id IS NOT NULL AND m.status = 1
QUERY;

  // if option "Status All Mocks" is disabled, returns only mocks with status active
  if ($all_mock_status_value == 2) {
    $sql_get_all_mocks .= " AND m.status = 1";
  }

  $resource = db_query($sql_get_all_mocks);

  $output = '';

  while($result = db_fetch_array($resource)) {
    $output .= '<br /><h2>Mock - '.$result['mock_title'].'</h2>';

    $header_table_mock = array(
      l('success', 'mockapi/'.$result['mock_title'].'/success', array('attributes' => array('target' => '_blank'))),
      l('failure', 'mockapi/'.$result['mock_title'].'/failure', array('attributes' => array('target' => '_blank'))),
      l('random', 'mockapi/'.$result['mock_title'].'/random', array('attributes' => array('target' => '_blank'))),
    );

    $output .= theme_table($header_table_mock, array());
  }

  return $output;
}

/**
 * Implemented of _services_mock_return_check_return_type
 * @param <string> $type_return
 * @param <string> $content
 * @return <boolean> $return
 */
function _services_mock_return_check_return_type($type_return = NULL, $content = NULL) {
  if (is_null($type_return) || is_null($content)) {
    return FALSE;
  }

  $type_return = drupal_strtolower($type_return);

  switch($type_return) {
    case 'json' :
      $json = json_decode($content);
      $return = (is_null($json)) ? FALSE : TRUE;
      break;
    case 'xml' :
      $xml = @simplexml_load_string($content);
      $return = ($xml !== FALSE) ? TRUE : FALSE;
      break;
    case 'plain' :
      $plain_text = check_plain($content);
      $return = ($plain_text == '') ? FALSE : TRUE;
      break;
  }

  return $return;
}

/**
 * Implementation of _services_mock_status_return_type
 * Return status list for return type of mock
 * @return <array> $status
 */
function _services_mock_status_return_type() {
  $status = array(
    'success' => t('Success'),
    'failure' => t('Failure'),
    'random'  => t('Random'),
  );

  return $status;
}


/**
 * Implementation of _services_mock_return_success_failure
 * Return the "mock return", success and failure of the clicked return
 * @param <integer> $mock_id
 * @param <integer> $return_type_id
 * @param <string>  $type (Type of return. Eg: Json, XML ....)
 * @return <array> in theme 'modal-return-type'
 */
function _services_mock_return_success_failure($mock_id = NULL, $return_type_id = NULL, $type = NULL) {
  if (is_null($mock_id) || is_null($return_type_id) || is_null($type) ||
      !is_numeric($mock_id) || !is_numeric($return_type_id)) {
    return NULL;
  }

  // Mock return Types
  $sql_get_mock_return_types = "SELECT mock_return.*, mock_mock.title as mock_title
                                FROM {mock_return} LEFT JOIN  {mock_mock} ON mock_mock.id = mock_return.mock_id
                                WHERE mock_id = " . $mock_id . " AND mock_return_type_id = " . $return_type_id . "
                                ORDER BY title";

  $result_query = db_query($sql_get_mock_return_types);
  $results_return_types = db_fetch_array($result_query);

  if ($results_return_types) {
    $results_return_types['type_return'] = $type;
    print theme('modal-return-type', $results_return_types);
  }

}


/**
 * Implementation of _services_mock_truncate_check_text
 * Get the "$text" and checks if with the "$size" was truncated.
 * If yes returns the text, else return FALSE
 * @param <string> $text
 * @param <integer> $size
 */
function _services_mock_truncate_check_text($text = NULL, $size = 20) {
  if (is_null($text)) {
    return FALSE;
  }

  $name_str_truncated = truncate_utf8($text, $size, FALSE, TRUE);
  $name_was_truncated = (drupal_strlen($name_str_truncated) < drupal_strlen($text));

  return ($name_was_truncated) ? $name_str_truncated : FALSE;
}
