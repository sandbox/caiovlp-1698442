<?php
/**
 * Mock admin inc file.
 */

/**
 * Implements mock admin form
 */
function form_mock_category_add() {
  // fieldset for including new category
  $form['new_category'] = array(
    '#type' => 'fieldset',
    '#title' => t('New category'),
    '#collapsible' => TRUE,
  );

  $form['new_category']['form_mock_category_title'] = array(
    '#type' => 'textfield',
    '#description' => t('Category´s title.'),
    '#title' => t('Title'),
    '#required' => TRUE,
    '#size' => 60,
    '#maxlength' => 200,
    '#element_validate' => array('form_validate_mock_category_title_unique'),
  );

  $form['new_category']['form_mock_category_note'] = array(
    '#type' => 'textarea',
    '#description' => t('Category´s note.'),
    '#title' => t('Note'),
    '#required' => TRUE,
    '#size' => 60,
    '#maxlength' => 200,
    '#element_validate' => array('form_validate_mock_category_note_unique'),
  );

  $form['new_mock']['form_mock_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function form_mock_category_add_submit($form, &$form_state) {
  $title = $form_state['values']['form_mock_category_title'];
  $note = $form_state['values']['form_mock_category_note'];

  $data = array(
    'title' => $title,
    'note' => $note,
  );

  drupal_write_record('mock_category', $data);

  drupal_set_message(t('Category added successfully!'));
}

function form_mocs() {

  $form['new_mock']['form_response'] = array(
    '#type' => 'textarea',
    '#description' => t('Response'),
    '#title' => t('Response'),
    '#required' => TRUE,
    '#cols' => 60,
    '#rows' => 8,
  );
}
