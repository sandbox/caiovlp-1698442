<div class="mock-doc">
  <h2>Documentation</h2>

  <br />
  <h3><strong>Using as function</strong></h3>
  <code>
    array mock ( string $mock_title )
  </code>
  <h3>
    Parameter
  </h3>
  <ul>
    <li>
      Title name (mandatory)
    </li>
  </ul>
  <p>
    Return:<br />
    An associative array:
    <pre>
      $return_data = array(
        'status' => 'SERVICE_DOWN',
        'type' => 'plain',
        'data' => $service_down_content['return'],
        'message' => '[NOTICE] Return for service down state',
      );
    </pre>
  </p>

  <p>
    Status:
  <ul>
    <li>SERVICE_DOWN - Return for service down state</li>
    <li>ALL_OFF - All mocks are set off</li>
    <li>OFF - This Mock either is off or do not exists</li>
    <li>OK - Successfully data return</li>
  </ul>

  </p>

  <h4>code</h4>
  <pre class="php">
    <code>
      &#60;&#63;php
        $return = mock('teste_1');
      &#63;&#62;
    </code>
  </pre>

  <br />
  <h3><strong>Using API :</strong></h3>
  <h4>code</h4>
  <p>You can call API without second parameter. In this case API will return what is recorded in the database</p>
  <pre class="php">
    <code>
      &#60;&#63;php
        $return = file_get_contents(%URL% . 'mockapi/teste_1');
      &#63;&#62;
    </code>
  </pre>
  <p>Also you can call API with a second parameter for which  type we want. Options are success, failure and random</p>
  <pre>
    <code>
      &#60;&#63;php
        $return = file_get_contents(%URL% . 'mockapi/teste_1/success');
      &#63;&#62;
    </code>
  </pre>
  <h3>
    Parameters
  </h3>
  <ul>
    <li>
      <ul>First parameter
        <li>Title name (mandatory)</li>
      </ul>
    </li>
    <li>
      <ul>Second parameter (optional)
        <li>success</li>
        <li>failure</li>
        <li>random</li>
      </ul>
    </li>
  </ul>

  <br />
  <h3><strong>Flow status of mocks :</strong></h3>
  <?php
    $path_images = drupal_get_path('module', 'services_mock') . '/images';
    $path_images = url($path_images);
  ?>
  <img src="<?php print $path_images; ?>/flow_status_mocks.png" alt="Flow status of mocks" title="Flow status of mocks"/>

</div>
