<?php
/**
 * Mock admin inc file.
 */

/**
 * Implementation of services_mock_category_verify
 * @param <string> $category_id
 * @return <array>  $form
 */
function services_mock_category_verify($category_id) {
  $form = array();
  $form['category_id'] = array(
    '#type' => 'hidden',
    '#value' => $category_id,
  );

  $mocks = array();
  $mocks = _services_mock_get_mock_by_category($category_id);
  if ($category_id == 1) {
  	drupal_set_message('The category Miscelaneous cannot be erased');
  	drupal_goto('admin/settings/mock/category');
  }
  if (count($mocks) > 0) {
    if (count($mocks) == 1) {
      $message = t('One mock was found with this category. In order to delete this category, please set another category for this mock. ');
    }else{
      $message = count($mocks). t(' mocks were found  with this category. In order to delete this category, please set another category to these mocks.');
    }

    $form['mock_category_warning'] = array(
      '#value' => $message,
    );

    /**
    * Here I get all categories except the one that i'm excluding
    * because the user need to choose a new category before
    * excluding it
    */
    $options = services_mock_category_list_except_this_id($category_id);
    $form['new_category_id'] = array(
    	'#type' => 'select',
      '#description' => t('Choose a new category for this mock'),
      '#title' => t('Category'),
      '#options' => $options,
      '#default_value' => 1,
      '#required' => TRUE,
    );

    $form['mock_category_last_warning'] = array(
      '#value' => t('Do you really want delete this category?'),
    );

    $form['mock_delete_yes'] = array(
      '#type' => 'submit',
      '#value' => t('Yes'),
      '#submit' => array('services_mock_category_verify_submit'),
    );

    $form['mock_delete_no'] = array(
      '#type' => 'submit',
      '#value' => t('No'),
      '#submit' => array('services_mock_category_verify_submit'),
    );
  }
  else {
   _services_mock_delete_category_by_id($category_id);
  }
  return $form;
}

/**
 * Implementation of  hook form submit
 * @param <type> $form
 * @param <type> $form_state
 */
function services_mock_category_verify_submit($form, &$form_state) {
  // if click button "mock_delete_no"
  if ($form_state['clicked_button']['#id'] == 'edit-mock-delete-no') {
    drupal_goto('admin/settings/mock/category');
  }

  /**
  * Verifing the need to change some mock´s category
  */
  $category_id = $form_state['values']['category_id'];
  $new_category_id = $form_state['values']['new_category_id'];
  $sql_update_mocks_category = "UPDATE {mock_mock} SET `category_id` = " . $new_category_id . " WHERE category_id = " . $category_id;
  db_query($sql_update_mocks_category);
  _services_mock_delete_category_by_id($category_id);
}

/**
 * Implementation of services_mock_category_list_except_this_id
 * @param <integer> $category_id
 * @return <array> $options_categories
 */
function services_mock_category_list_except_this_id($category_id) {
  $return = array();
  $sql_get_all_mocks_except_one_id = "SELECT `id`, `title` FROM {mock_category} WHERE id != " . $category_id;
  $results = db_query($sql_get_all_mocks_except_one_id);
  $options_categories = array();
  while($result = db_fetch_array($results)) {
    $options_categories[$result['id']] = t($result['title']);
  }

   return $options_categories;
}

/**
 * Implementation of _services_mock_get_mock_by_category
 * @param <integer> $category_id
 * @return <array> $return
 */
function _services_mock_get_mock_by_category($category_id) {
  $return = array();
  $sql_get_all_mocks_by_category = "SELECT `id`, `title`, `note`, `category_id`  FROM {mock_mock} where category_id = " . $category_id;
  $results = db_query($sql_get_all_mocks_by_category);

  while($result = db_fetch_array($results)) {
    $return[] = $result;
  }

  return $return;
}
