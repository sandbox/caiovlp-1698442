<?php
// $Id$

/**
 * import Mock Content
 * @file
 */

/********* import CONTENT FORM *********************/

/**
 * Implementation of Hook_FORM()
 * @param  <array> $form_state
 * @return <array>
 */
function services_mock_import_content_form(&$form_state) {
  $form = array();

  $form['title'] = array(
    '#type'  => 'markup',
    '#value' => '<h2>' . t('Import Mocks') . '</h2>',
  );

  $form['upload_sql_mocks'] = array(
    '#type'          => 'file',
    '#title'         => t('Upload a Backup File'),
    '#description'   => t('Upload a file "Mock Content DB", created by this module. The imported content, are SQL statements. Max file size: 2 MB.'),
    '#size'          => 60,
    '#attributes' => array(
      'class'   => 'import-upload'
    )
  );

  $form['description'] = array(
    '#type' => 'markup',
    '#value' => t('<p>Always test your backups on a non-production server!</p>'),
  );

  $form['import_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import All Mocks'),
  );

  $form['#attributes'] = array('enctype' => 'multipart/form-data');
  return $form;
}

/**
 * Implementation of Hook_FORM_Submit()
 * @param <type> $form
 * @param <type> $form_state
 */
function services_mock_import_content_form_submit($form, &$form_state) {
  $validators['services_mock_import_file_validators'] = array();
  if ($file = file_save_upload('upload_sql_mocks', $validators)) {
    module_load_include('inc', 'services_mock', 'inc/export_import_db/services_mock_import_export_content');
    _services_mock_import_file_content($file->filepath);
    watchdog('mock_import_db', 'Mock Contents imported from upload %file', array('%file' => $file->filename));
  }
  else {
    form_set_error('upload_sql_mocks', t('Upload a Backup File field is required'));
  }
}

/**
 * Implementation of services_mock_import_file_validators
 * @param <type> $form
 * @param <type> $form_state
 */
function services_mock_import_file_validators($file) {
  global $base_path;
  $errors = array();

  if ($file->filemime != 'application/octet-stream') {
    $url_import = l('Export DB Mock', 'admin/settings/mock/options/export');
    $errors[] = t('File selected is invalid');
    $errors[] = t('Please select only SQL files, exported in the mock in !url_import', array('!url_import' => $url_import));
  }

  return $errors;
}




