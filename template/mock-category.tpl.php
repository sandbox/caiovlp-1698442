<?php
// $Id$

/*******************************************************************************
* Copyright (c) 2010, HealthMedia, Inc.
* All Rights Reserved.
* This software is proprietary to HealthMedia, Inc.
* Your access to this software is governed by the terms of your license agreement with HealthMedia, Inc.
* Any other use of the software is strictly prohibited.
*******************************************************************************/

/**
 * @file
 */
?>
<?php print $form; ?>
<div id="category_list_div">
<table>
<tr><th>Title</th></tr>
<?php $count = FALSE; ?>
<?php foreach($category_list as $category) { ?>
<tr class ="<?php if ($count == TRUE) { ?>
	<?php print 'even';  ?>
	<?php  } else {?>
	<?php print 'odd';?>
	<?php }?>
	" >
  <td colspan="<?php ($category['id'] == 1) ? print 3 : print 0 ?>">
    <?php print $category['title'];  ?>
  </td>
  <?php if ($category['id'] > 1) { ?>
    <td>
      <a href ="<?php print base_path();?>admin/settings/mock/category/<?php print $category['id'] ?>" > edit</a>
    </td>
    <td>
      <a href ="<?php print base_path();?>admin/settings/mock/category/<?php print $category['id'] ?>/verify" > delete</a>
    </td>
  <?php } ?>
</tr>
<?php $count = !$count;?>
<?php }?>
</table>
</div>
