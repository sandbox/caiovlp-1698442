<?php

/**
 * Implementation of services_mock_list_output
 * Returns the content of mock list
 * @return <array> $form drupal
 */
function services_mock_list_output() {
  $module_path = drupal_get_path('module', 'services_mock');

  // Addd highlight plugin for (Success / Failure) Fields
  drupal_add_css($module_path . '/js/plugin_highlight/highlight.css');
  drupal_add_js($module_path . '/js/plugin_highlight/highlight.js');

  drupal_add_js(array('base_path' => base_path()), 'setting');
  drupal_add_js($module_path . '/js/services_mock_list_mock.js');

  return drupal_get_form('services_mock_list_form');
}

/**
 * Implementation of Hook form
 * @param <array> $form
 * @return <array> $form
 */
function services_mock_list_form($form) {
  $form = array();
  $mocks = services_mock_list();

  if (count($mocks) == 0) {
    $form['no-mock-registered'] = array(
      '#type'   => 'markup',
      '#value'  => '<p class="no-mock-registered">' . t('There are not registered mocks') . '</p>',
    );

    return $form;
  }

  $form['messsage-status-changed-option'] = array(
    '#type'    => 'item',
    '#value'   => '',
    '#prefix'  => '<div id="message-status-changed-option">',
    '#suffix'  => '</div>'
  );

  $all_mock_status_value = variable_get('STATUS_ALL_MOCKS', 0);
  $all_mock_status_value = is_null($all_mock_status_value) ? 0 : $all_mock_status_value;

  $form['all_mock_status'] = array(
    '#type'    => 'select',
    '#title'   => t('Status All Mocks'),
    '#options' => array(
      0 => t('Disabled'),
      1 => t('On'),
      2 => t('Off'),
    ),
    '#default_value' => isset($form['#post']['all_mock_status']) ?  $form['#post']['all_mock_status'] : $all_mock_status_value,
    '#attributes'    => array('class' => 'check-all-mock-status'),

  );

  foreach ($mocks as $mock) {
    $form['mock_category_' . $mock['id']] = array(
      '#type'        => 'fieldset',
      '#title'       => check_plain(t($mock['title'])),
      '#collapsible' => TRUE,
    );

    foreach ($mock['mocks'] as $m) {
      $mock_collapsed  = TRUE;
      // When selecting a mock and to change its status or the status of their returns, keeps the contents of the mock open
      if (isset($_SESSION['mock_collapsed_id']) && $_SESSION['mock_collapsed_id'] == $m['id']) {
        $mock_collapsed  = FALSE;
        unset($_SESSION['mock_collapsed_id']);
      }

      $form['mock_category_' . $mock['id']]['mock_' . $m['id']] = array(
        '#type'        => 'fieldset',
        '#title'       => check_plain(t($m['title'])),
        '#collapsible' => TRUE,
        '#collapsed'   => $mock_collapsed,
      );


      $form['mock_category_' . $mock['id']]['mock_' . $m['id']]['id_mock'] = array(
        '#type'  => 'hidden',
        '#value' => $m['id'],
        '#attributes' => array('class' => 'id-mock'),
      );

      $form['mock_category_' . $mock['id']]['mock_' . $m['id']]['form_mock_status_' . $m['id']] = array(
        '#type'    => 'select',
        '#title'   => t('Status'),
        '#options' => array(
          1 => t('On'),
          0 => t('Off'),
        ),
        '#default_value' => $m['status'],
        '#attributes' => array('class' => 'form-mock-status'),
      );

      if (!empty($m['note'])) {
        $form['mock_category_' . $mock['id']]['mock_' . $m['id']]['note_' . $m['id']] = array(
          '#type'   => 'markup',
          '#value'  => '<label>' . t('Observation Note:') . '</label>',
          '#prefix' => '<div class="form-item observation-note">',
          '#suffix' => t('!string', array('!string' => $m['note'])) . '</div>',
        );
      }

      $form['mock_category_' . $mock['id']]['mock_' . $m['id']]['add_return' . $m['id']] = array(
        '#type'   => 'markup',
        '#value'  => '<strong>' . l(t('Add Return'), 'admin/settings/mock/return/add-new-return/' . $m['id'] , array('attributes' => array('class' => 'add-return', 'title' => 'Add Return'))) . '</strong>',
        '#suffix' => (isset($m['return_type'])) ? '<div class="form-item"><label>' . t('Return Types:') . '</div>' : '',
      );

      if (isset($m['return_type'])) {
        foreach ($m['return_type'] as $key => $rtype) {

          if ($rtype['return_type_active']) {
            $status = t('Active');
            $class_status = 'mock-return-active';
          }
          else {
            $status = t('Inactive');
            $class_status = 'mock-return-inactive';
          }

          $form['mock_category_' . $mock['id']]['mock_' . $m['id']]['Return Type ' . $key] = array(
            '#type'        => 'fieldset',
            '#title'       => check_plain($rtype['title']) . ' (' . check_plain(drupal_strtoupper(t($rtype['type_return']))) . ')&nbsp;&nbsp;&nbsp;[' . $status . ']',
            '#collapsible' => TRUE,
            '#collapsed'   => TRUE,
            '#attributes'  => array('class' => $class_status),
          );

          $form['mock_category_' . $mock['id']]['mock_' . $m['id']]['Return Type ' . $key]['id_mock'] = array(
            '#type'  => 'hidden',
            '#value' => $rtype['mock_id'],
            '#attributes' => array('class' => 'id-mock'),
          );

          $form['mock_category_' . $mock['id']]['mock_' . $m['id']]['Return Type ' . $key]['id_return_mock'] = array(
            '#type'  => 'hidden',
            '#value' => $rtype['id'],
            '#attributes' => array('class' => 'id-return-mock'),
          );

          $form['mock_category_' . $mock['id']]['mock_' . $m['id']]['Return Type ' . $key]['id_return_type_mock'] = array(
            '#type'  => 'hidden',
            '#value' => $rtype['mock_return_type_id'],
            '#attributes' => array('class' => 'id_return_type_mock'),
          );

          if (!$rtype['return_type_active']) {
            $form['mock_category_' . $mock['id']]['mock_' . $m['id']]['Return Type ' . $key]['set_active_status'] = array(
              '#type'        => 'button',
              '#value'       => t('Set Active Status'),
              '#attributes'  => array('class' => 'set-active-status btn-set-active-status'),
            );
          }

          if (!empty($rtype['note'])) {
            $form['mock_category_' . $mock['id']]['mock_' . $m['id']]['Return Type ' . $key]['note'] = array(
              '#type'   => 'markup',
              '#value'  =>  '<label>' . t('Note') . ': </label>',
              '#prefix' => '<div class="form-item">',
              '#suffix' => t($rtype['note']) . '</div>',
            );
          }

          // Mock Return Status
          module_load_include('inc', 'services_mock', 'services_mock');
          $options_return_status = _services_mock_status_return_type();

          $form['mock_category_' . $mock['id']]['mock_' . $m['id']]['Return Type ' . $key]['return_status'] = array(
            '#type'          => 'select',
            '#title'         => t('Return Status'),
            '#options'       => $options_return_status,
            '#required'      => TRUE,
            '#attributes'    => array('class' => 'return-status'),
            '#default_value' => $rtype['return_status'],
          );

          // Success Code
          $form['mock_category_' . $mock['id']]['mock_' . $m['id']]['Return Type ' . $key]['success'] = array(
            '#type'        => 'fieldset',
            '#title'       =>  t('Success'),
            '#collapsible' => TRUE,
            '#collapsed'   => TRUE,
            '#attributes'  => array('class' => 'mock-return-code'),
          );

          $form['mock_category_' . $mock['id']]['mock_' . $m['id']]['Return Type ' . $key]['success']['code'] = array(
            '#type'  => 'markup',
            '#value' =>  '<pre class="code" lang="' . $rtype['type_return'] . '">' . t($rtype['success']) . '</pre>',
          );

          // Failure Code
          $form['mock_category_' . $mock['id']]['mock_' . $m['id']]['Return Type ' . $key]['failure'] = array(
            '#type'        => 'fieldset',
            '#title'       => t('Failure'),
            '#collapsible' => TRUE,
            '#collapsed'   => TRUE,
            '#attributes'  => array('class' => 'mock-return-code'),
          );

          $form['mock_category_' . $mock['id']]['mock_' . $m['id']]['Return Type ' . $key]['failure']['code'] = array(
            '#type'  => 'markup',
            '#value' =>  '<pre class="code" lang="' . $rtype['type_return'] . '">' . t($rtype['failure']) . '</pre>',
          );
        }
      }
      else {
        // Check if the mock  have return type
        $form['mock_category_' . $mock['id']]['mock_' . $m['id']]['form_mock_status_' . $m['id']]['#attributes'] = array('disabled' => 'disabled', 'oncontextmenu' => 'return false');
        $form['mock_category_' . $mock['id']]['mock_' . $m['id']]['form_mock_status_' . $m['id']]['#description'] = '<label>' . t('Please add a return to change status') . '</label>';
      }
    }
  }
  return $form;
}

/**
 * Implementation of services_mock_list_set_status
 * Set Active/ Inactive Status for all mocks, a Specific Mock or Return Types of Mock
 * Obs: If variable "$check_type_status" is "all_mocks" and your status is TRUE,
 *      it will prevail on the status of each mock
 *
 * @return <json>
 */
function services_mock_list_set_status() {
  $check_type_status = $_POST['check_type_status'];
  $status            = $_POST['status'];
  $id_mock           = (isset($_POST['id_mock']) && is_numeric($_POST['id_mock'])) ? $_POST['id_mock'] : NULL;

  $results_mock_change_status = FALSE;

  if (isset($check_type_status) && isset($status)) {

    if ($check_type_status == 'all_mocks') {  // change status for all mock
      variable_set('STATUS_ALL_MOCKS', $status);
      $results_mock_change_status = TRUE;
    }
    elseif ($check_type_status == 'status_return_type') {
      $id_return_type = (isset($_POST['id_return']) && !empty($_POST['id_return']) && is_numeric($_POST['id_return'])) ? $_POST['id_return'] : NULL;

      if (!is_null($id_return_type)) {
        $sql_update_status = "UPDATE {mock_return} SET return_status = '" . $status . "'
                              WHERE mock_id = " . $id_mock . " AND mock_return_type_id = " . $id_return_type;

        $results = db_query($sql_update_status);
        $_SESSION['mock_collapsed_id'] = $id_mock;
        $results_mock_change_status = TRUE;
      }
    }
    elseif (!is_null($id_mock)) {
      $id_return = (isset($_POST['id_return']) && !empty($_POST['id_return']) && is_numeric($_POST['id_return'])) ? $_POST['id_return'] : NULL;

      $data = array(
        'id' => $id_mock,
        'status' => $status,
      );

      if ($check_type_status == 'return_type' && !is_null($id_return)) {
        $data['return_id'] = $id_return;
      }
      elseif ($check_type_status == 'mock') {
        //return drupal_json(array('status' => $data));
      }

      $results_mock_change_status = drupal_write_record('mock_mock', $data, 'id');
      if ($results_mock_change_status !== FALSE) {
        $results_mock_change_status = TRUE;
        $_SESSION['mock_collapsed_id'] = $id_mock;
      }
    }

  }
  return ($results_mock_change_status) ? drupal_json(array('response' => array('success' => TRUE))) : drupal_json(array('response' => array('success' => FALSE)));
}

/**
 * Implementation of services_mock_list
 * Return the content of database for list
 * @return <array> $return
 */
function services_mock_list() {
  $return = array();
  $sql_get_mocks_category = <<<QUERY
    SELECT DISTINCT mock_category.id, mock_category.title, mock_category.note from {mock_category}
    JOIN {mock_mock} ON mock_category.id = mock_mock.category_id
    ORDER BY mock_category.title ASC
QUERY;

  $results_categories = db_query($sql_get_mocks_category);

  while($result = db_fetch_array($results_categories)) {
    $return[$result['id']] = $result;
    $sql_get_all_mocks = <<<QUERY
      SELECT mock_mock.id, mock_mock.category_id, mock_mock.title, mock_mock.note, mock_mock.status, mock_mock.return_id
      FROM {mock_mock}
      WHERE mock_mock.category_id = {$result['id']}
      ORDER BY mock_mock.category_id
QUERY;
    $results_mocks = db_query($sql_get_all_mocks);
    while($mocks = db_fetch_array($results_mocks)) {
      $return[$result['id']]['mocks'][] = $mocks;
    }

    foreach($return[$result['id']]['mocks'] as $key_mock => $values) {
      // Mock return Types
      $sql_get_mock_return_types = <<<QUERY
        SELECT mr.*, mrt.title as type_return from {mock_return} AS mr
        JOIN {mock_return_type} AS mrt ON mr.mock_return_type_id = mrt.id
        WHERE mr.mock_id = {$values['id']}
        ORDER BY mrt.title
QUERY;
      $results_return_types = db_query($sql_get_mock_return_types);
      if ($results_return_types) {

        while ($return_types = db_fetch_array($results_return_types)) {
          $return_type_item =  $return_types + array('return_type_active' => FALSE);

          // if "return" is [Active]
          if (!is_null($values['return_id']) && $values['return_id'] == $return_types['id']) {
            $return_type_item['return_type_active']  = TRUE;

            // if "return" is [Active], then your content will be the first of list retuns
            if (is_array(isset($return[$result['id']]['mocks'][$key_mock]['return_type']))) {
              array_unshift($return[$result['id']]['mocks'][$key_mock]['return_type'], $return_type_item);
            }
            else {
              // if "return" is the first element of array
              $return[$result['id']]['mocks'][$key_mock]['return_type'][] = $return_type_item;
            }
          }
          else {
            $return[$result['id']]['mocks'][$key_mock]['return_type'][] = $return_type_item;
          }
        }

      }
    }
  }

  return $return;
}

