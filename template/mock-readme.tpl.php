<div class="mock-readme">
  <h2>CHANGELOG</h2>

  <h3>Version 2.9 [CURRENT]</h3>
  <p>URL: <?php echo l('Release 2.9', 'https://svn2.cit.com.br/repos/jnj_scm_pod/JJWNP/Bison/trunk/doc/Architecture/MockApp/tags/release-2.9'); ?></p>
  <ul>
    <li>Add ahah functionality to change ('Status All Mock', 'Status Mock', 'Return Status') in the Mock List</li>
    <li>Mock system refactory</li>
    <li>Add some unit tests (simpletest module)</li>
  </ul>

  <h3>Version 2.3</h3>
  <ul>
    <li>Created a return for when the service is down (Generic)</li>
    <li>Sort returns active first</li>
    <li>Switch on/off all mocks (keeps mock with no returns off and keeps last state)</li>
    <li>Flowchart explaining how mocks works [doc]</li>
    <li>Success as default</li>
    <li>Validate returns before saving (XML / Json)</li>
    <li>Import / Export mocks as SQL statements</li>
    <li>Refactory of mock API</li>
  </ul>

  <h3>Version 1</h3>
  <ul>
    <li>Manage mocks</li>
    <li>Manage categories</li>
    <li>Add return</li>
    <li>Switch mock status</li>
    <li>Manage default return</li>
  </ul>

  <h2>NEXT RELEASE</h2>
  <h3>Version 3</h3>
  <ul>
    <li>Add mocks automatically</li>
    <li>Refactory (always)</li>
    <li>Add more unit tests (Simpletest)</li>
    <li>User interface improvement</li>
    <li>Add custom return type (Something like a plugin or hook)</li>
  </ul>

  <h2>KNOWN BUGS</h2>
  </div>
