<?php
/*
 * Created on 26/01/2012
 *
 * set of functions that generate the form for return types, saves and alter them in the data basel
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

/**
 * Implementation of _services_mock_return_type_add_form_output
 * @param <type> $form
 * @param <type> $id
 * @param <type> $delete
 * @return <type>
 */
function _services_mock_return_type_add_form_output($form, $id = NULL, $delete = NULL) {
  $form = drupal_get_form('services_mock_return_type_add_form', $id, $delete);

  module_load_include('inc', 'services_mock', 'services_mock');
  if (count(services_mock_return_type_list()) == 0) {
    return $form;
  }
  return theme('mock-return-type', $form, services_mock_return_type_list());
}

/**
 * Implementation of services_mock_return_type_add_form
 * @param <type> $form
 * @param <type> $id
 * @param <type> $delete
 * @return <type>
 */
function services_mock_return_type_add_form($form, $id = NULL, $delete = NULL) {
  $form = array();

  if (!is_null($id)) {
    $return_type = array();
    $return_type = _services_mock_get_return_type_by_id($id);
    $form['id'] = array(
      '#type' => 'hidden',
      '#value' => $id,
    );
    $form_collapsed = FALSE;
  }
  else {
    $form_collapsed = TRUE;
  }

  if (!is_null($delete)) {
    $form_collapsed = TRUE;
    _services_mock_delete_return_type_by_id($id);
    $return_type = NULL;
    $form['id']['#value'] = NULL;
  }

  // fieldset for including new return type
  $form['new_return_type'] = array(
    '#type' => 'fieldset',
    '#title' => t('New Return Type'),
    '#collapsible' => TRUE,
    '#collapsed' => $form_collapsed,
  );

  $form['new_return_type']['return_type_title']= array(
    '#type' => 'textfield',
    '#description' => t('The name you will use for your return. Do not use spaces.'),
    '#title' => t('Return Type Title'),
    '#required' => TRUE,
    '#default_value' => isset($return_type['title']) ? $return_type['title'] : '',
    '#size' => 60,
    '#maxlength' => 200,
  );

  $form['new_return_type']['return_type_notes']= array(
    '#type' => 'textarea',
    '#description' => t('Observation Notes'),
    '#title' => t('Return Type Notes'),
    '#required' => FALSE,
    '#default_value' => isset($return_type['note']) ? $return_type['note'] : '',
    '#cols' => 60,
    '#rows' => 8,
  );

  $form['new_return_type']['return_type_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function _services_mock_get_return_type_by_id($id) {
  $return = array();
  $sql_get_all_mocks_return_types = "SELECT `id`, `title`, `note` FROM {mock_return_type} WHERE id = " . $id;
  $result = db_query($sql_get_all_mocks_return_types);
  $return = db_fetch_array($result);

  return $return;
}

function _services_mock_delete_return_type_by_id($id) {
  $return = array();
  $sql_delete_mocks_return_types = "DELETE FROM {mock_return_type} WHERE id=" . $id;
  db_query($sql_delete_mocks_return_types);
}

function services_mock_return_type_add_form_submit($form, &$form_state) {
  $title = $form_state['values']['return_type_title'];
  $note = $form_state['values']['return_type_notes'];

  $data = array(
    'title' => $title,
    'note' => $note,
   );

  if (isset($form_state['values']['id'])) {
    $data['id'] = $form_state['values']['id'];

    $return_type_save = drupal_write_record('mock_return_type', $data, 'id');
    $path = 'admin/settings/mock/return-type';
    drupal_goto($path);
  }
  else {
    $return_type_save = drupal_write_record('mock_return_type', $data);
  }

  if ($return_type_save !== FALSE) {
    drupal_set_message(t('Return Type added successfully!'));
  }
}
